#version 450
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal;
out vec3 fragmentNormal;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;

void main() {
	fragmentNormal = vertexNormal;
	gl_Position = MVP * vec4(vertexPosition_modelspace,1);
}