#version 420                                            // Keeping you on the bleeding edge!
#extension GL_EXT_gpu_shader4 : enable

smooth in vec3 fragmentNormal;

// Force location to 0 to ensure its the first output
layout (location = 0) out vec4 o_FragColor;

struct Lights
{
  vec3 position;
  vec3 diffuse; // Colour
};

float lambert(vec3 N, vec3 L)
{
  vec3 nrmN = normalize(N);
  vec3 nrmL = normalize(L);
  float result = dot(nrmN, nrmL);
  return max(result, 0.0);
}

void main()
{
  // Temporary light, exam one will be a uniform input
  Lights light;
  light.position = vec3(1,3,-1.7);
  light.diffuse = vec3(0.6, 0.7, 0.5);
  //

  vec3 result = light.diffuse * lambert(fragmentNormal, light.position);

  o_FragColor = vec4(result.rgb, 1.0);
}