#include "stdafx.h"
#include "Geometry.h"

#include "Vertex.h"
#include "Face.h"
#include "Edge.h"
#include "MeshZone.h"
#include <glm/glm.hpp>

#include <algorithm>
#include <vector>
#include "IDGenerator.h"

using namespace glm;

Geometry::Geometry(Vertex* vertexList,
	int vertexCount,
	Edge* edgeList,
	int edgeCount,
	Face* faceList,
	int faceCount
	) {
	this->vertexList = vertexList;
	this->edgeList = edgeList;
	this->faceList = faceList;

	this->faceCount = faceCount;
	this->vertexCount = vertexCount;
	this->edgeCount = edgeCount;

	this->faceCap = faceCount;
	this->vertexCap = vertexCount;
	this->edgeCap = edgeCount;
};

void Geometry::resize(int divisionIterations) {
	Geometry oldGeom = *this;

	Vertex* vList = vertexList;
	Edge* eList = edgeList;
	Face* fList = faceList;

	for (int i = 0; i < divisionIterations; i++) {
		int oldVCap = vertexCap;
		int oldECap = edgeCap;
		int oldFCap = faceCap;

		vertexCap = oldVCap + oldECap + oldFCap;
		edgeCap = oldECap * 2 + oldFCap * 4;
		faceCap = oldFCap * 4;
	}

	// Increases size of arrays
	vertexList = new Vertex[vertexCap];
	std::copy(vList, vList + vertexCount, vertexList);
	delete[] vList;

	edgeList = new Edge[edgeCap];
	std::copy(eList, eList + edgeCount, edgeList);
	delete[] eList;

	faceList = new Face[faceCap];
	std::copy(fList, fList + faceCount, faceList);
	delete[] fList;

	//Updates references since all objects have been moved into the new list
	// in a different memory location
	for (int i = 0; i < vertexCount; i++) {
		vertexList[i].updateRefs(oldGeom, *this);
	}
	for (int i = 0; i < edgeCount; i++) {
		edgeList[i].updateRefs(oldGeom, *this);
	}
	for (int i = 0; i < faceCount; i++) {
		faceList[i].updateRefs(oldGeom, *this);
	}
}


GLfloat* Geometry::createVertexBuffer() {
	GLfloat* vertexData = new GLfloat[faceCount * 6 * 3];

	for (int i = 0; i < faceCount; i++) {
		Face* f = &faceList[i];

		vec3 triVerts[6];
		
		triVerts[0] = f->verts[0]->val;
		triVerts[1] = f->verts[1]->val;
		triVerts[2] = f->verts[2]->val;

		triVerts[3] = f->verts[2]->val;
		triVerts[4] = f->verts[3]->val;
		triVerts[5] = f->verts[0]->val;

		// Vertex number = 6*i + j
		for (int j = 0; j < 6; j++) {
			for (int k = 0; k < 3; k++) {
				vertexData[3 * (6 * i + j) + k] = triVerts[j][k];
			}
		}
	}
	return vertexData;
}

GLfloat* Geometry::createNormalBuffer() {
	GLfloat* normalData = new GLfloat[faceCount * 6 * 3];

	for (int i = 0; i < faceCount; ++i) {
		Face* f = &faceList[i];

		vec3 triVerts[6];
		triVerts[0] = f->verts[0]->val;
		triVerts[1] = f->verts[1]->val;
		triVerts[2] = f->verts[2]->val;

		triVerts[3] = f->verts[2]->val;
		triVerts[4] = f->verts[3]->val;
		triVerts[5] = f->verts[0]->val;

		vec3 norm[2];
		norm[0] = normalize(cross(triVerts[2] - triVerts[0], triVerts[1] - triVerts[0]));
		norm[1] = normalize(cross(triVerts[5] - triVerts[3], triVerts[4] - triVerts[3]));

		//cout << norm[0].x << ", " << norm[0].y << ", " << norm[0].z << "\n";
		//cout << norm[1].x << ", " << norm[1].y << ", " << norm[1].z << "\n";

		for (int j = 0; j < 6; ++j) {
			for (int k = 0; k < 3; ++k)
			{
				if (j / 3 == 0) {
					normalData[3 * (6 * i + j) + k] = norm[0][k];
				}
				else {
					normalData[3 * (6 * i + j) + k] = norm[1][k];
				}
			}
		}
	}
	return normalData;
}

/*
Performs subdivision once on the specified region and replaces the
given MeshZone object with a smaller version representing the fully-subdivided region
(Overwriting the MeshZone object is fine since even if you didn't, the faces it points to
	have changed, making it no longer valid)
*/
void Geometry::subdivideRegion(MeshZone* meshZone, int oldDepth) {
	if (meshZone->innerFaces.size() == 0) {
		// No zone to work with
		return;
	}

	// Gets geometry object data
	Vertex* vList = this->vertexList;
	Edge* eList = this->edgeList;
	Face* fList = this->faceList;
	int newVIdx = this->vertexCount;
	int newEIdx = this->edgeCount;
	int newFIdx = this->faceCount;

	std::vector<Face*> newInnerFaces = std::vector<Face*>();
	int markId = IDGenerator::instance()->next();
	int markId2 = IDGenerator::instance()->next();

	// Calculate face-points, split edges, and adjust vertex positions
	for (int i = 0; i < meshZone->innerFaces.size(); i++) {
		Face* faceRef = meshZone->innerFaces[i];

		// There is a small chance that the zone has already been fully subdivided
		if (faceRef->divisionDepth != oldDepth) {
			continue;
		}

		// In order to allow subdivide-regions to overlap by one face,
		// we need to make sure face-point and edges aren't computed twice
		bool isPartiallySplit = (faceRef->faceV != nullptr);
		if (!isPartiallySplit) {
			faceRef->calcFaceVertex(&vList[newVIdx]);
			newVIdx++;
		}

		for (int j = 0; j < 4; j++) {
			Edge* edgeRef = faceRef->edges[j];
			if (edgeRef->tmpId != markId && edgeRef->tmpId != markId2) {
				// First time this edge has been encountered
				if (isPartiallySplit) {
					// Mark edge as incident with a partially processed face
					edgeRef->tmpId = markId2;
				}
				else {
					// Mark edge as incident with a normal face
					edgeRef->tmpId = markId;
				}
			}
			else {
				if (isPartiallySplit && edgeRef->tmpId == markId2) {
					// Edge lies between two partially split faces, which
					// means it has already been split (or should have been)
					continue;
				}

				// Second time edge has been encountered, so both neighboring faces
				// have had their face points calculated
				edgeRef->splitEdge(&vList[newVIdx], &eList[newEIdx]);
				newVIdx++;
				newEIdx += 3;

				// Vertices get adjusted automatically by the split-edge function
			}
		}
	}

	// Create new face objects, add face references to edges, and create new mesh-zone
	for (int i = 0; i < meshZone->innerFaces.size(); i++) {
		Face* faceRef = meshZone->innerFaces[i];
		int origDepth = faceRef->divisionDepth;

		if (origDepth != oldDepth) {
			newInnerFaces.push_back(faceRef);
			continue;
		}

		int newFaceCount = faceRef->splitFacePartial(&fList[newFIdx]);
		// Add newly created faces to new inner zone vector
		// and update the face index
		for (int j = 0; j < newFaceCount; j++) {
			newInnerFaces.push_back(&fList[newFIdx]);
			newFIdx++;
		}

		if (faceRef->divisionDepth != origDepth) {
			// If the face's division depth has changed, then this object is
			// being reused and now represents a sub-face
			// So we should add it as well
			newInnerFaces.push_back(faceRef);
		}
	}

	// Update MeshZone object
	*meshZone = MeshZone(newInnerFaces);

	//Update active object trackers
	this->vertexCount = newVIdx;
	this->edgeCount = newEIdx;
	this->faceCount = newFIdx;
}

void Geometry::subdivideRest(int targetDepth) {
	Vertex* vList = this->vertexList;
	Edge* eList = this->edgeList;
	Face* fList = this->faceList;
	int newVIdx = this->vertexCount;
	int newEIdx = this->edgeCount;
	int newFIdx = this->faceCount;

	// Determines lowest subdivision depth
	int lowestDepth = fList[0].divisionDepth;
	for (int i = 1; i < newFIdx; i++) {
		if (this->faceList[i].divisionDepth < lowestDepth) {
			lowestDepth = fList[i].divisionDepth;
		}
	}

	for (; lowestDepth < targetDepth; lowestDepth++) {
		// Calculates face-points for faces of the lowest depth which have
		// not had their face points calculated yet
		for (int i = 0; i < newFIdx; i++) {
			Face* faceRef = &fList[i];
			if (faceRef->divisionDepth == lowestDepth) {
				if (faceRef->faceV == nullptr) {
					faceRef->calcFaceVertex(&vList[newVIdx]);
					newVIdx++;
				}

				// Splits bounding edges if they haven't been split yet
				for (int j = 0; j < 4; j++) {
					Edge* edgeRef = faceRef->edges[j];
					if(edgeRef->divisionDepth == lowestDepth && edgeRef->f1->faceV != nullptr && edgeRef->f2->faceV != nullptr){
						edgeRef->splitEdge(&vList[newVIdx], &eList[newEIdx]);
						newVIdx++;
						newEIdx += 3;
					}
				}
			}
		}

		// Assert that all edges at this level have been split
		for (int i = 0; i < newEIdx; i++) {
			Edge* edgeRef = &eList[i];
			assert(edgeRef->divisionDepth != lowestDepth);
		}

		// Creates new sub-faces
		int origSize = newFIdx;
		for (int i = 0; i < origSize; i++) {
			Face* faceRef = &fList[i];
			if (faceRef->divisionDepth == lowestDepth) {
				int newFaceCount = faceRef->splitFacePartial(&fList[newFIdx]);
				newFIdx += newFaceCount;

				// At this point the face should have been subdivided
				assert(faceRef->divisionDepth != lowestDepth);
			}
		}

		for (int i = 0; i < newEIdx; i++) {
			Edge* edgeRef = &eList[i];
			if (edgeRef->divisionDepth <= lowestDepth + 1) {
				assert(edgeRef->f1 != nullptr && edgeRef->f2 != nullptr);
			}
		}
	}

	//Update active object trackers
	this->vertexCount = newVIdx;
	this->edgeCount = newEIdx;
	this->faceCount = newFIdx;
}

int Geometry::getUnmarkedFace() {
	for (int i = 0; i < faceCount; ++i) {
		if (faceList[i].tmpId == 0) {
			return i;
		}
	}
	return -1;
}

Geometry::~Geometry()
{
}
