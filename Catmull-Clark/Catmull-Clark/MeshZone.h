#pragma once

class Vertex;
class Edge;
class Face;
class Geometry;

#include <vector>

class MeshZone
{
public:
	std::vector<Face*> innerFaces;

	MeshZone(Face* startFace);
	MeshZone(std::vector<Face*> innerFaces);
	static std::vector<MeshZone> generateMeshZones(Geometry* geomObj, uint16_t iterCount, float cacheSize);
	static float estimateCacheSizeForPerfectSquare(float squareWidth, int iterCount);
	bool expand();
	void updateReferences(Geometry* geomObj, Geometry* oldGeomObj);

	~MeshZone();
private:
	int id;
};

