#pragma once

class Vertex;
class Edge;
class Face;
class MeshZone;

/* Fairly simple container class for the vertex, edge, and face lists that define
a geometric mesh, along with the size of each list.*/
class Geometry
{
public:
	Vertex* vertexList;
	Edge* edgeList;
	Face* faceList;

	int vertexCount;
	int edgeCount;
	int faceCount;
	int vertexCap;
	int edgeCap;
	int faceCap;

	Geometry(Vertex* vertexList,
		int vertexCount,
		Edge* edgeList,
		int edgeCount,
		Face* faceList,
		int faceCount
		);
	void resize(int divisionIterations);
	GLfloat* createVertexBuffer();
	GLfloat* createNormalBuffer();

	int getUnmarkedFace();

	void subdivideRegion(MeshZone* meshZone, int oldDepth);
	void subdivideRest(int targetDepth);

	~Geometry();
};

