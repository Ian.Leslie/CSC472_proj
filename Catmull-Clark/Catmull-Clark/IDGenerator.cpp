#include "stdafx.h"
#include "IDGenerator.h"

IDGenerator* IDGenerator::only_copy = nullptr;

IDGenerator::IDGenerator() {
	_id = 1;
}

int IDGenerator::next() {
	return _id++;
}

IDGenerator* IDGenerator::instance() {
	if (!only_copy) {
		only_copy = new IDGenerator();
	}
	return only_copy;
};