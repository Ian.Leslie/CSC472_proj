#include "stdafx.h"
#include "Face.h"
#include <assert.h>

#include "Vertex.h"
#include "Edge.h"
#include "Geometry.h"

Face::Face(Vertex* v1, Vertex* v2, Vertex* v3, Vertex* v4, int divDepth) {
	verts[0] = v1;
	verts[1] = v2;
	verts[2] = v3;
	verts[3] = v4;
	for (int i = 0; i < 4; i++) {
		edges[i] = nullptr;
		edgeHalfs[i] = nullptr;
		internalEdges[i] = nullptr;
		hasSubFace[i] = false;
	}
	faceV = nullptr;
	tmpId = 0;
	divisionDepth = divDepth;
};

void Face::updateRefs(Geometry oldGeom, Geometry newGeom) {
	for (int i = 0; i < 4; i++) {
		if (verts[i] != nullptr) {
			verts[i] = verts[i] - oldGeom.vertexList + newGeom.vertexList;
		}
		if (edges[i] != nullptr) {
			edges[i] = edges[i] - oldGeom.edgeList + newGeom.edgeList;
		}
		if (internalEdges[i] != nullptr) {
			internalEdges[i] = internalEdges[i] - oldGeom.edgeList + newGeom.edgeList;
		}
		if (edgeHalfs[i] != nullptr) {
			edgeHalfs[i] = edgeHalfs[i] - oldGeom.edgeList + newGeom.edgeList;
		}
	}
	if (faceV != nullptr) {
		faceV = faceV - oldGeom.vertexList + newGeom.vertexList;
	}
}

Vertex* Face::calcFaceVertex(Vertex* out) {
	*out = Vertex((verts[0]->val + verts[1]->val + verts[2]->val + verts[3]->val) / 4.0f);
	faceV = out;

	for (int i = 0; i < 4; i++) {
		verts[i]->addToFaceAcum(faceV->val);
	}

	return faceV;
}

// Returns a neighbouring face object
// idx = 0,1,2,3
Face* Face::getAdjacentFace(int idx) {
	Edge* boundE = edges[idx];
	if (boundE->f1 == this) {
		return boundE->f2;
	}
	else {
		return boundE->f1;
	}
}

void Face::registerEdgeSplit(Edge* boundEdge, Edge* internalEdge, Edge* otherHalf) {
	for (int i = 0; i < 4; i++) {
		if (edges[i] == boundEdge) {
			internalEdges[i] = internalEdge;
			edgeHalfs[i] = otherHalf;
			return;
		}
	}

	// Edge could not be found among boundary edges
	assert(false);
}


Edge* Face::getEdgeHalfTo(int idx, Vertex* endP) {
	Edge* edgeA = edges[idx];
	Edge* edgeB = edgeHalfs[idx];
	if (edgeA->v1 == endP || edgeA->v2 == endP) {
		return edgeA;
	}
	else {
		assert(edgeB->v1 == endP || edgeB->v2 == endP);
		return edgeB;
	}
}


int Face::getSubFaceCount() {
	int faceCount = 0;
	for (int i = 0; i < 4; i++) {
		if (hasSubFace[i]) {
			faceCount++;
		}
	}
	return faceCount;
}

// Assumes that the necessary data is there and face has not already been split
Face Face::getSubFaceFor(int idx) {
	Vertex* v1 = internalEdges[(idx + 3) % 4]->getOtherVert(faceV);
	Vertex* v3 = internalEdges[idx]->getOtherVert(faceV);

	Face newFace = Face(faceV,
		v1,
		verts[idx],
		v3,
		divisionDepth + 1
		);

	newFace.edges[0] = internalEdges[(idx + 3) % 4];
	newFace.edges[1] = getEdgeHalfTo((idx + 3) % 4,verts[idx]);
	newFace.edges[2] = getEdgeHalfTo(idx, verts[idx]);
	newFace.edges[3] = internalEdges[idx];

	return newFace;
}

/*
Creates as many sub-faces as possible and returns the # of new face objects created
Takes into account that face may have already been partially split.

NOTE: For the final 4th face, this object gets re-used to store it.
So the function returning 0 does not necessarily mean nothing happened.
This behaviour isn't ideal since it is kind of confusing, but for the purposes of this
algorithm it should be fine.
*/
int Face::splitFacePartial(Face* faceOutArr) {
	int createdFaces = 0;
	int totalFaces = getSubFaceCount();

	for (int i = 0; i < 4; i++) {
		if (!hasSubFace[i] && internalEdges[(i + 3) % 4] != nullptr && internalEdges[i] != nullptr) {
			totalFaces++;
			hasSubFace[i] = true;
			if (totalFaces == 4) {
				// For last face, re-use this object
				*this = getSubFaceFor(i);
				updateEdgeRefs();
			}
			else {
				*faceOutArr = getSubFaceFor(i);
				faceOutArr->updateEdgeRefs();
				faceOutArr++;
				createdFaces++;
			}
		}
	}

	// Returns number of newly created face objects so the array pointer can be updated
	return createdFaces;
}

/*
Creates all 4 sub faces

All uses of "splitFace" could be replaced by "splitFacePartial" calls,
but then there would be a bunch of unecessary conditions checked.
*/
void Face::splitFace(Face* faceOutArr) {
	assert(getSubFaceCount() == 0);
	for (int i = 0; i < 4; i++) {
		assert(internalEdges[i] != nullptr);
	}

	for (int i = 0; i < 3; i++) {
		*faceOutArr = getSubFaceFor(i);
		faceOutArr->updateEdgeRefs();
		faceOutArr++;
	}
	*this = getSubFaceFor(3);
	updateEdgeRefs();
}

void Face::updateEdgeRefs() {

	for (int i = 0; i < 4; i++) {
		// Get bounding edge
		Edge* eRef = edges[i];

		// Add this face to the edge's face reference
		assert(eRef->f1 != this && eRef->f2 != this);
		if (eRef->f1 == nullptr) {
			eRef->f1 = this;
		}
		else {
			assert(eRef->f2 == nullptr);
			eRef->f2 = this;
		}
	}
}

Face::~Face()
{
}
