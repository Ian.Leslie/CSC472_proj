#include "stdafx.h"
#include "Vertex.h"

#include "Edge.h"
#include "Face.h"
#include "Geometry.h"

using namespace glm;

Vertex::Vertex() {
	Vertex(vec3(0.0f, 0.0f, 0.0f),false);
}

Vertex::Vertex(vec3 v, bool isSharp, int valence) {
	val = v;
	sharpEdgeAcum = vec3(0.0f, 0.0f, 0.0f);
	sharpEdgeC = 0;
	edgeAcum = vec3(0.0f, 0.0f, 0.0f);
	adjEdgeC = 0;
	faceAcum = vec3(0.0f, 0.0f, 0.0f);
	adjFaceC = 0;
	this->valence = valence;
	tmpId = 0;
	this->isSharp = isSharp;
};

void Vertex::updateRefs(Geometry oldGeom, Geometry newGeom) {
	// References might be added later, so it's handy to keep this function setup
}

void Vertex::addToEdgeAcum(vec3 v, bool isSharp) {
	if (isSharp) {
		sharpEdgeAcum = sharpEdgeAcum + v;
		sharpEdgeC++;
	}
	else{
		edgeAcum = edgeAcum + v;
		adjEdgeC++;
	}
};

void Vertex::addToFaceAcum(vec3 v) {
	faceAcum = faceAcum + v;
	adjFaceC++;
};

void Vertex::adjustPosition() {
	
	// Using: http://xrt.wikidot.com/blog:31 for rules regarding sharpness
	if (isSharp || sharpEdgeC > 2) {
		// Don't move point at all
	}
	else if (sharpEdgeC == 2) {
		// Crease rule
		val = val*(3.0f / 4.0f) + sharpEdgeAcum*(1.0f / 8.0f);
	}
	else {
		float n = float(valence);

		vec3 faceAverage = faceAcum*(1.0f / float(adjFaceC));
		vec3 edgeAverage = (edgeAcum + sharpEdgeAcum) * (1.0f / n);
		// Smooth rule
		val = faceAverage*(1.0f / n) + edgeAverage*(2.0f / n) + val*((n - 3.0f) / n);
	}

	edgeAcum = vec3(0.0f, 0.0f, 0.0f);
	adjEdgeC = 0;
	sharpEdgeAcum = vec3(0.0f, 0.0f, 0.0f);
	sharpEdgeC = 0;
	faceAcum = vec3(0.0f, 0.0f, 0.0f);
	adjFaceC = 0;
}

Vertex::~Vertex()
{
}
