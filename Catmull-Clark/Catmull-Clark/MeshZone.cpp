#include "stdafx.h"
#include "MeshZone.h"

#include <cmath>
#include <iostream>

#include "Vertex.h"
#include "Edge.h"
#include "Face.h"
#include "Geometry.h"
#include "IDGenerator.h"
#include <Windows.h>

#include <assert.h>

using namespace std;

//const uint64_t zoneSoftCapSize = 16;

MeshZone::MeshZone(Face* startFace)
{
	innerFaces = vector<Face*>();
	innerFaces.push_back(startFace);
	id = IDGenerator::instance()->next();
	startFace->tmpId = -1 * id;
}

MeshZone::MeshZone(vector<Face*> innerFaces)
{
	this->innerFaces = innerFaces;
	id = IDGenerator::instance()->next();
}

float MeshZone::estimateCacheSizeForPerfectSquare(float squareWidth, int iterCount) {
	float memUsed = 0;
	for (int i = 0; i < iterCount; i++) {
		float newFaces = squareWidth*squareWidth * 3.0;
		float originalEdgeCount = 2.0 * squareWidth*(squareWidth + 1.0);
		float splitableEdges = originalEdgeCount - 4.0 * squareWidth;
		float newEdges = splitableEdges * 3.0;
		float newVertices = squareWidth*squareWidth + splitableEdges;

		memUsed += float(sizeof(Face))*newFaces + float(sizeof(Edge))*newEdges + float(sizeof(Vertex))*newVertices;

		squareWidth = squareWidth * 2 - 2;
	}

	return memUsed;
}

vector<MeshZone> MeshZone::generateMeshZones(Geometry * geomObj, uint16_t iterCount, float cacheSize)
{
	int sqWidth = 1;
	// This method of determining the face-count is kind of ineffecient,
	// but estimateCacheSizeFor is a pretty simple function, so I don't think it matters much
	while (estimateCacheSizeForPerfectSquare(float(sqWidth), iterCount) < cacheSize) {
		sqWidth++;
	}
	int targetFaceCount = (sqWidth-1)*(sqWidth-1);
	for (; estimateCacheSizeForPerfectSquare(sqrt(targetFaceCount), iterCount) < cacheSize; targetFaceCount++) {
	}
	targetFaceCount--;
	if (targetFaceCount < 0) {
		targetFaceCount = 1;
	}

	cout << "Target Cache Size: " << cacheSize << "\n";
	cout << "Iterations:        " << iterCount << "\n";
	cout << "Face Count Target: " << targetFaceCount << "\n";
	cout << "Actual Estimated Cache Size: " << estimateCacheSizeForPerfectSquare(sqrt(targetFaceCount), iterCount) << "\n";

	vector<MeshZone> theZones = vector<MeshZone>();
	int index;
	while ((index = geomObj->getUnmarkedFace()) >= 0) {
		MeshZone zone(&geomObj->faceList[index]);
		bool changed = true;
		while (zone.innerFaces.size() < targetFaceCount && changed) {
			changed = zone.expand();
		}
		theZones.push_back(zone);
	}
	cout << "Created all zones\n";
	return theZones;
}

bool MeshZone::expand() {
	bool changed = false;
	//int markId = IDGenerator::instance()->next();

	// Mark all original faces and their vertices
	for (int i = 0; i < innerFaces.size(); i++) {
		innerFaces[i]->tmpId = id;
		for (int j = 0; j < 4; j++) {
			innerFaces[i]->verts[j]->tmpId = id;
		}
	}

	// Perform basic expansion of zone, so all faces touching
	// bounding edges are included if not already in a meshZone
	size_t originalSize = innerFaces.size();
	for (size_t i = 0; i < originalSize; i++) {
		for (int j = 0; j < 4; j++) {
			Face* faceRef = innerFaces[i]->getAdjacentFace(j);
			if (faceRef->tmpId <= 0 && faceRef->tmpId != -1*id) {
				changed = true;
				faceRef->tmpId = -1 * id;
				innerFaces.push_back(faceRef);
			}
		}
	}

	// Add "corner" faces that don't share an original edge, but
	// do share an original vertex
	for (size_t i = originalSize; i < innerFaces.size(); i++) {
		for (int j = 0; j < 4; j++) {
			Face* faceRef = innerFaces[i]->getAdjacentFace(j);
			if (faceRef->tmpId <= 0 && faceRef->tmpId != -1*id) {
				changed = true;
				bool hasMarkedVertex = false;
				for (int k = 0; k < 4; k++) {
					if (faceRef->verts[k]->tmpId == id) {
						hasMarkedVertex = true;
						break;
					}
				}
				if (hasMarkedVertex) {
					faceRef->tmpId = -1 * id;
					innerFaces.push_back(faceRef);
				}
			}
		}
	}
	return changed;
}


void MeshZone::updateReferences(Geometry* geomObj, Geometry* oldGeomObj) {
	for (int i = 0; i < innerFaces.size(); i++) {
		innerFaces[i] = innerFaces[i] - oldGeomObj->faceList + geomObj->faceList;
	}
}

MeshZone::~MeshZone()
{
}
