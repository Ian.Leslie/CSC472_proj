#pragma once
// Slightly adapted from
//http://stackoverflow.com/questions/1988679/algorithm-for-generating-a-unique-id-in-c
class IDGenerator {
public:
	static IDGenerator* instance();
	static IDGenerator* only_copy;
	int next();
private:
	IDGenerator();

	int _id;
};