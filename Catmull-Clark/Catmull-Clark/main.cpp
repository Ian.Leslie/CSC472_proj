//main.cpp : Defines the entry point for the console application.
//Window management code borrowed from opengl-tutorial.org
//
#include "stdafx.h"

#include <assert.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Vertex.h"
#include "Edge.h"
#include "Face.h"
#include "Geometry.h"
#include "MeshZone.h"
#include "IDGenerator.h"

using namespace std;
using namespace glm;

GLFWwindow* window;

void performSubdivisionIteration(Geometry* geomObj) {
	cout << "Entered\n";

	// Note: These counters track how many objects are currently in the array
	//		not how many objects there are room for
	int vCount = geomObj->vertexCount;
	int eCount = geomObj->edgeCount;
	int fCount = geomObj->faceCount;

	Vertex* vList = geomObj->vertexList;
	Edge* eList = geomObj->edgeList;
	Face* fList = geomObj->faceList;
	
	cout << "resize done\n";

	// Note that although the arrays are now larger, the vCount,eCount, & fCount
	// variables will still frequently be used since they refer to the region
	// with the orignal data.

	// Creates a new face point for each face
	for (int i = 0; i < fCount; i++) {
		fList[i].calcFaceVertex(&vList[vCount + i]);
		// Note: At this stage, the face point is missing edge data
	}

	cout << "Face Points added\n";

	// Creates edge points (and automatically adjusts vertices)
	for (int i = 0; i < eCount; i++) {
		eList[i].splitEdge(&vList[i + vCount + fCount], &eList[3 * i + eCount]);
	}

	cout << "Edge points created (and vertices adjusted)\n";

	// Now we just need to create the new faces and link them to the edges
	for (int i = 0; i < fCount; i++) {
		fList[i].splitFace(&fList[3 * i + fCount]);
	}

	for (int i = 0; i < eCount * 2 + fCount * 4; i++) {
		assert(eList[i].f1 != nullptr);
		assert(eList[i].f2 != nullptr);
	}

	geomObj->vertexCount = vCount + eCount + fCount;
	geomObj->edgeCount = eCount * 2 + fCount * 4;
	geomObj->faceCount = fCount * 4;

	cout << "Subdivision Iteration done\n";
}

GLuint LoadShaders(const char* vertex_file_path, const char* fragment_file_path) {
	//Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open()) {
		std::string Line = "";
		while (std::getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	} else {
		printf("Impossible to open %s.\n", vertex_file_path);
		getchar();
		return 0;
	}
	
	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open()) {
		std::string Line = "";
		while (getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}else {
		printf("Impossible to open %s.\n", vertex_file_path);
		getchar();
		return 0;
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}


	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

void performRegionalSubdivisionOn(Geometry* cubeGeo, int startingDepth, int totalIterations, float cacheSize) {
	cout << "Target Cache Size: " << cacheSize << "\n";

	// Calculates maximum # of iterations to before at once
	int iterGroup = 1;
	for (; iterGroup < totalIterations; iterGroup++) {
		if (MeshZone::estimateCacheSizeForPerfectSquare(1.0, iterGroup) > cacheSize) {
			if (iterGroup > 1) {
				iterGroup--;
			}
			break;
		}
	}

	// Performs total number of iterations by splitting it into cycle where each
	// cycle does no more than the maximum number of iterations
	int iterationsDone = 0;
	int cycleCount = 0;
	while (iterationsDone < totalIterations) {
		int iterationsThisCycle = min(iterGroup, totalIterations - iterationsDone);

		vector<MeshZone> zones = MeshZone::generateMeshZones(cubeGeo, iterationsThisCycle, cacheSize);
		for (size_t index = 0; index < zones.size(); ++index) {
			for (int i = 0; i < iterationsThisCycle; i++) {
				cubeGeo->subdivideRegion(&zones.at(index));
			}
		}

		cubeGeo->subdivideRest(iterationsDone + iterationsThisCycle + startingDepth);
		iterationsDone += iterationsThisCycle;
		cycleCount++;
	}

	cout << "Max Iterations: " << iterGroup << "\n";
	cout << iterationsDone << " iterations done in " << cycleCount << " cycles.\n";
}

int main( void )
{
	// Initialize GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(1024, 1024, "Catmull-Clark", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window.\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glewExperimental = GL_TRUE;
	// Initialize GLEW
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders("../../shaders/simple.vert", "../../shaders/simple.frag");

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	// Projection matrix : 45� Field of View, 1:1 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(45.0f, 1.0f, 0.1f, 100.0f);
	// Camera matrix
	glm::mat4 View = glm::lookAt(
		glm::vec3(3, 2, -2), // Camera position in World Space
		glm::vec3(0, 0, -0.75), // looking at
		glm::vec3(0, 1, 0)  // Head is up
	);
	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model = glm::mat4(1.0f);

	glm::mat4 MVP = Projection * View * Model;

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	/////// Constructs vertex, edge, and face lists to represent the
	/////// geometry of of a cube.
	float val = 0.75f;
	float val2 = 0.75f;
	int vCount = 8;
	Vertex* vertexList = new Vertex[vCount];
	vertexList[0] = Vertex(vec3(val, val, val - val2),   false, 3);
	vertexList[1] = Vertex(vec3(-val, val, val - val2),  false, 3);
	vertexList[2] = Vertex(vec3(-val, -val, val - val2), false, 3);
	vertexList[3] = Vertex(vec3(val, -val, val - val2),  false, 3);

	vertexList[4] = Vertex(vec3(val, val, -val - val2),   false, 3);
	vertexList[5] = Vertex(vec3(-val, val, -val - val2),  false, 3);
	vertexList[6] = Vertex(vec3(-val, -val, -val - val2), false, 3);
	vertexList[7] = Vertex(vec3(val, -val, -val - val2),  false, 3);

	Vertex* v = vertexList;
	int fCount = 6;
	Face* faceList = new Face[fCount];
	faceList[0] = Face(&v[0], &v[3], &v[2], &v[1]);
	faceList[1] = Face(&v[4], &v[5], &v[6], &v[7]);
	faceList[2] = Face(&v[0], &v[4], &v[7], &v[3]);
	faceList[3] = Face(&v[1], &v[2], &v[6], &v[5]);
	faceList[4] = Face(&v[0], &v[1], &v[5], &v[4]);
	faceList[5] = Face(&v[3], &v[7], &v[6], &v[2]);

	
	Face* f = faceList;
	int eCount = 12;
	Edge* edgeList = new Edge[eCount];
	edgeList[0] = Edge(&v[0], &v[1], &f[0], &f[4]);
	edgeList[1] = Edge(&v[1], &v[2], &f[0], &f[3]);
	edgeList[2] = Edge(&v[2], &v[3], &f[0], &f[5]);
	edgeList[3] = Edge(&v[3], &v[0], &f[0], &f[2]);

	edgeList[4] = Edge(&v[4], &v[5], &f[1], &f[4]);
	edgeList[5] = Edge(&v[5], &v[6], &f[1], &f[3]);
	edgeList[6] = Edge(&v[6], &v[7], &f[1], &f[5]);
	edgeList[7] = Edge(&v[7], &v[4], &f[1], &f[2]);

	edgeList[8]  = Edge(&v[0], &v[4], &f[2], &f[4]);
	edgeList[9]  = Edge(&v[1], &v[5], &f[4], &f[3]);
	edgeList[10] = Edge(&v[2], &v[6], &f[3], &f[5]);
	edgeList[11] = Edge(&v[3], &v[7], &f[5], &f[2]);

	// Link up face edge references
	// (this is an inefficient way to do it, but since it's part of the initialization it doesn't
	//		really matter)
	for (int i = 0; i < fCount; i++) {
		Face* faceRef = &faceList[i];
		for (int j = 0; j < 4; j++) {
			Vertex* v1 = faceRef->verts[j];
			Vertex* v2 = faceRef->verts[(j+1) % 4];
			for (int k = 0; k < eCount; k++) {
				Edge* edgeRef = &edgeList[k];
				if (edgeRef->getOtherVert(v1) == v2 && edgeRef->getOtherVert(v2) == v1) {
					faceRef->edges[j] = edgeRef;
					break;
				}
			}
		}
	}

	for (int i = 0; i < 4; i++) {
		faceList[2].edges[i]->isSharp = true;
	}

	////////// End of model construction ///////////

	Geometry* cubeGeo = new Geometry(vertexList, vCount,
		edgeList, eCount,
		faceList, fCount );

	// Do subdivision
	int totalIterations = 8;//5;
	int basicIterations = 0;
	//bool renderPartial = true;

	cubeGeo->resize(totalIterations);
	for (int i = 0; i < basicIterations; i++) {
		performSubdivisionIteration(cubeGeo);
	}

	// Use target cache size of 256KB
	float cacheSize = float(256*(2 << 9));
	performRegionalSubdivisionOn(cubeGeo, basicIterations, totalIterations - basicIterations, cacheSize);

	/*
	MeshZone mZone = MeshZone(&cubeGeo->faceList[1]);
	MeshZone mZone2 = MeshZone(&cubeGeo->faceList[0]);
	mZone.expand();
	mZone2.expand();
	//mZone.expand();
	for (int i = basicIterations; i < totalIterations; i++) {
		cubeGeo->subdivideRegion(&mZone);
	}
	for (int i = basicIterations; i < totalIterations; i++) {
		cubeGeo->subdivideRegion(&mZone2);
	}*/

	/*
	if (!renderPartial) {
		cubeGeo->subdivideRest(totalIterations);
	}*/
	
	/*
	assert(cubeGeo->vertexCount == cubeGeo->vertexCap);
	assert(cubeGeo->edgeCount == cubeGeo->edgeCap);
	assert(cubeGeo->faceCount == cubeGeo->faceCap);
	*/

	GLfloat* g_vertex_buffer_data = cubeGeo->createVertexBuffer();
	GLfloat* g_normal_buffer_data = cubeGeo->createNormalBuffer();

	/*const GLfloat g_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		-1.0f, 0.5f, 0.0f,
		1.0f, 0.5f, 0.0f,
		0.0f, 0.0f, 0.0f,
		-0.5f, 1.0f, 0.0f,
		0.5f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
	};*/


	// Uncomment to just render lines
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	GLuint n_vertices = cubeGeo->faceCount * 6;
	GLuint vertexbuffer;

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, n_vertices * 3 * sizeof(GLfloat), g_vertex_buffer_data, GL_STATIC_DRAW);

	GLuint normalBuffer;

	glGenBuffers(1, &normalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, n_vertices * 3 * sizeof(GLfloat), g_normal_buffer_data, GL_STATIC_DRAW);
	

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Black background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	do {
		// Clear the screen.
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();

		// DO STUFF HERE
		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			1,                  // attribute 1. No particular reason for 1, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		glUseProgram(programID);

		glDrawArrays(GL_TRIANGLES, 0, n_vertices); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	}// Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

    return 0;
}

