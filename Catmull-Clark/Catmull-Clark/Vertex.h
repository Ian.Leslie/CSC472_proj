#pragma once

class Edge;
class Face;
class Geometry;

#include "glm/vec3.hpp"

class Vertex
{
public:
	glm::vec3 val;
	glm::vec3 edgeAcum; //temp variable used for calculation
	int adjEdgeC;
	glm::vec3 sharpEdgeAcum; //temp variable used for calculation
	int sharpEdgeC;
	glm::vec3 faceAcum; //temp variable used for calculation
	int adjFaceC;
	int valence;
	int tmpId;
	bool isSharp;

	Vertex();
	Vertex(glm::vec3 v, bool isSharp = false, int valence = 4);
	void updateRefs(Geometry oldGeom, Geometry newGeom);
	void addToEdgeAcum(glm::vec3 v, bool isSharp);
	void addToFaceAcum(glm::vec3 v);
	void adjustPosition();

	~Vertex();
};