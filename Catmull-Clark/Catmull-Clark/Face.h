#pragma once

class Vertex;
class Edge;
class Geometry;

#include "glm/vec3.hpp"

class Face
{
public:
	Vertex* verts[4];
	Edge* edges[4];
	Edge* edgeHalfs[4];
	Edge* internalEdges[4];
	bool hasSubFace[4];
	Vertex* faceV; //temp variable used for calculation
	glm::vec3 Normal;
	int tmpId;
	int divisionDepth;

	Face(Vertex* v1 = nullptr, Vertex* v2 = nullptr, Vertex* v3 = nullptr, Vertex* v4 = nullptr, int divDepth = 0);
	void updateRefs(Geometry oldGeom, Geometry newGeom);
	Vertex* calcFaceVertex(Vertex* out);

	Face* getAdjacentFace(int idx);

	void registerEdgeSplit(Edge* boundEdge, Edge* internalEdge, Edge* otherHalf);

	int getSubFaceCount();
	int splitFacePartial(Face* faceOutArr);
	void splitFace(Face* faceOutArr);
	void updateEdgeRefs();

	~Face();

private:
	Face getSubFaceFor(int idx);
	Edge* getEdgeHalfTo(int idx, Vertex* endP);

};

