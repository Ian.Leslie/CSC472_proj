#include "stdafx.h"
#include "Edge.h"
#include <assert.h>

#include "Vertex.h"
#include "Face.h"
#include "Geometry.h"

Edge::Edge(Vertex* v1, Vertex* v2, Face* f1, Face* f2, int divDepth) {
	this->v1 = v1;
	this->v2 = v2;
	this->f1 = f1;
	this->f2 = f2;
	tmpId = 0;
	isSharp = false;
	divisionDepth = divDepth;
};


void Edge::updateRefs(Geometry oldGeom, Geometry newGeom) {
	if (v1 != nullptr) {
		v1 = v1 - oldGeom.vertexList + newGeom.vertexList;
	}
	if (v2 != nullptr) {
		v2 = v2 - oldGeom.vertexList + newGeom.vertexList;
	}
	if (f1 != nullptr) {
		f1 = f1 - oldGeom.faceList + newGeom.faceList;
	}
	if (f2 != nullptr) {
		f2 = f2 - oldGeom.faceList + newGeom.faceList;
	}
}

void Edge::clearFaceData() {
	this->f1 = nullptr;
	this->f2 = nullptr;
}

void Edge::addFaceRef(Face* adjFace) {
	if (this->f1 == nullptr) {
		this->f1 = adjFace;
	}
	else {
		//this->f2 == nullptr
		assert(this->f2 == nullptr);
		this->f2 = adjFace;
	}
}

Vertex* Edge::getOtherVert(Vertex* v) {
	if (v == this->v1) {
		return this->v2;
	}
	else {
		return this->v1;
	}
}

Vertex* Edge::getEdgePoint(Vertex* v) {
	if (isSharp) {
		*v = Vertex((v1->val + v2->val)*(1.0f / 2.0f));
	}
	else {
		*v = Vertex((v1->val + v2->val + f1->faceV->val + f2->faceV->val)*(1.0f / 4.0f));
	}
	return v;
}

void Edge::splitEdge(Vertex* newPRef, Edge* edgeOutArr) {
	getEdgePoint(newPRef);

	// Adds edge point to incident (original) vertex accumalator
	if (isSharp) {
		v1->addToEdgeAcum(v2->val, isSharp);
		v2->addToEdgeAcum(v1->val, isSharp);
	}
	else
	{
		v1->addToEdgeAcum(newPRef->val, isSharp);
		v2->addToEdgeAcum(newPRef->val, isSharp);
	}

	if (v1->valence == v1->adjEdgeC + v1->sharpEdgeC) {
		v1->adjustPosition();
	}
	if (v2->valence == v2->adjEdgeC + v2->sharpEdgeC) {
		v2->adjustPosition();
	}

	Edge* origEdgeArr = edgeOutArr;

	// Connects edge point to neighbouring face points
	*edgeOutArr = Edge(newPRef, f1->faceV, nullptr, nullptr, divisionDepth + 1);
	edgeOutArr++;
	*edgeOutArr = Edge(newPRef, f2->faceV, nullptr, nullptr, divisionDepth + 1);
	edgeOutArr++;

	// Splits this edge into two connected to the new edge P
	bool isOriginalSharp = this->isSharp;

	*edgeOutArr = Edge(v2, newPRef, nullptr, nullptr, divisionDepth + 1);
	edgeOutArr->isSharp = isOriginalSharp;

	f1->registerEdgeSplit(this, &origEdgeArr[0], &origEdgeArr[2]);
	f2->registerEdgeSplit(this, &origEdgeArr[1], &origEdgeArr[2]);

	*this = Edge(v1, newPRef, nullptr, nullptr, divisionDepth + 1);
	isSharp = isOriginalSharp;
}

Edge::~Edge()
{
}
