#pragma once

class Vertex;
class Face;
class Geometry;

class Edge
{
public:
	Vertex *v1, *v2;
	Face *f1, *f2;
	int tmpId;
	bool isSharp;
	int divisionDepth;

	Edge(Vertex* v1 = nullptr, Vertex* v2 = nullptr, Face* f1 = nullptr, Face* f2 = nullptr, int divDepth = 0);
	void updateRefs(Geometry oldGeom, Geometry newGeom);
	void clearFaceData();
	void addFaceRef(Face* adjFace);
	Vertex* getOtherVert(Vertex* v);

	Vertex* getEdgePoint(Vertex* v);
	void splitEdge(Vertex* newPRef, Edge* edgeOutArr);

	~Edge();
};

