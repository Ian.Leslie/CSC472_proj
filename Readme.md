This is the CSC-472 project for Scott Byrne and Ian Leslie.
It will implement Catmull-Clark subdivision with an experimental preprocessing algorithm.

Install:
Update GPU drivers.
Copy the files in dll/ to C:/Windows/System32

If I have done everything correctly, that should be sufficient to run this with Visual Studio 2015.
If not, let me know and I will help with setup.
